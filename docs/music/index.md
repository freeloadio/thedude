Freeloadio Music API Overview
==============================

This document describes the API overview of the Freeloadio Music API.

Objects
========

Documentation on the Music Objects may be found at

- [Objects](objects.md)

Manifest
=========

The music manifest may be retriveved at:

`/api/v1/music.json`

This endpoint returns a JSON object manifest containing all artists, albums, and songs.

### Manifest Diagram

```mermaid
graph TD
  A[Artist] --> B[Album]
  B --> C[Album Art]
  B --> D[Song]
  D --> E[Song File]

  A[Artist]

  B[Album]

  C[Album Art]

  D[Song]

  E[Song File]
```
### Example JSON Response

Below is an example of a JSON response for music data:

```json
{
  "artists": [
    {
      "albums": [
        {
          "albumart": [
            {
              "format": "jpeg",
              "height": 1575,
              "role": "cover",
              "type": "AlbumArt",
              "url": "/media/archive/music/lorenzos_music/solamente_tres_palabras/cover.jpg",
              "width": 1575
            }
          ],
          "artist_slug": "lorenzos_music",
          "description": "<p>Our first album</p>",
          "license": "CC",
          "slug": "solamente_tres_palabras",
          "songs": [
            {
              "album_slug": "solamente_tres_palabras",
              "artist_slug": "lorenzos_music",
              "duration": 22.308571,
              "slug": "intro",
              "songfiles": {
                "mp3": {
                  "format": "mp3",
                  "type": "SongFile",
                  "url": "/media/archive/music/lorenzos_music/solamente_tres_palabras/Lorenzo's%20Music%20-%20Solamente%20Tres%20Palabras%20-%2001%20Intro.mp3"
                }
              },
              "title": "Intro",
              "tracknum": 1,
              "type": "Song"
            },
            ...
          ],
          "title": "Solamente Tres Palabras",
          "type": "Album"
        }
      ],
      "description": "<p>Lorenzo's Music is a band from Wisconsin</p>",
      "name": "Lorenzo's Music",
      "slug": "lorenzos_music",
      "type": "Artist"
    }
  ]
}
```

