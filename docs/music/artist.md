# Artist Object

Individuals, groups, or bands are represneted by artist objects.
 
Each artist object contains the following keys:

- `type`**: The type of object, which is "Album" to indicate this is an `Artist` object.
- **`slug`**: The unique identifier for the artist.
- **`name`**: The name of the artist.
- **`description`**: A long form description of the artist in plain text.
- **`albums`**: An array of [`Album`](album.md) objects associated with the artist.

## Example Artist Object

```json
{
  "albums": [...],
  "description": "Lorenzo's Music is a band from Wisconsin",
  "name": "Lorenzo's Music",
  "slug": "lorenzos_music",
  "type": "Artist"
}
```
