# Album Object

Each album object contains the following keys:

- **`type`**: The type of object, which is "Album" to indicate this is an `Album` object.
- **`slug`**: The unique identifier for the album. This must only be unique per artist.
- **`title`**: The title of the album.
- **`artist_slug`**: The unique identifier for the artist of this album.
- **`albumart`**: An array of album [AlbumArt](#albumart-object) objects.
- **`description`**: A description of the album in plain text.
- **`license`**: The license type of the album (e.g., "CC").
- **`songs`**: An array of [`Song`](song.md) objects associated with the album.

## Example Album Object

```json
{
  "albumart": [...],
  "artist_slug": "lorenzos_music",
  "description": "Our first album",
  "license": "CC",
  "slug": "solamente_tres_palabras",
  "songs": [...],
  "title": "Solamente Tres Palabras",
  "type": "Album"
}
```

# AlbumArt Object

Each AlbumArt Object has the following keys

- **`type`**: The type of object, which is "AlbumArt" to indicate that this is an `AlbumArt` object.
- **`role`**: The role of the album art. Currently the only value for this is "cover".
- **`format`**: The format of the image. This should be either "jpeg" or "png".
- **`height`**: The height of the image in pixels.
- **`width`**: The width of the image image in pixels.
- **`url`**: The URL at which the art may be retrieved.
- **`size`**: The size of the file in bytes
- **`sha256`**: The SHA256 digest of the file encoded in base64 without padding.

## Example AlbumArt Object


```json
{
  "format": "jpeg",
  "height": 1575,
  "role": "cover",
  "type": "AlbumArt",
  "url": "/media/archive/music/lorenzos_music/solamente_tres_palabras/cover.jpg",
  "width": 1575,
  "sha256": "F CVZLaQy1OnX84zcVD0xk9xLzqKF9NzlGxzKthdP1w",
  "size": 1036756
}
```