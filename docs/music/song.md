# Song Object

Each song object contains the following keys:

- **`type`**: The type of object, which is "Album" to indicate this is a `Song` object.
- **`slug`**: The unique identifier for the song within the album.
- **`title`**: The title of the song.
- **`tracknum`**: The order in which this song appears on the album.
- **`duration`**: The duration of the song in seconds as represented by a float.
- **`album_slug`**: The unique identifier for the album.
- **`artist_slug`**: The unique identifier for the artist.
- **`songfiles`**: An array of [`SongFile`](#songfile-object) objects associated with the song.

## Example Song Object

```json
{
  "album_slug": "solamente_tres_palabras",
  "artist_slug": "lorenzos_music",
  "duration": 22.308571,
  "slug": "intro",
  "songfiles": [
    {
      "format": "mp3",
      "type": "SongFile",
      "url": "/media/archive/music/lorenzos_music/solamente_tres_palabras/Lorenzo's%20Music%20-%20Solamente%20Tres%20Palabras%20-%2001%20Intro.mp3",
      "sha256": "F CVZLaQy1OnX84zcVD0xk9xLzqKF9NzlGxzKthdP1w",
      "size": 1036756
    }
  ],
  "title": "Intro",
  "tracknum": 1,
  "type": "Song"
}
```

# SongFile Object

A SongFile object represents a file containing the song in an encoding. 

- **`type`** : The type of object, which is "SongFile" to indicate this is an `SongFile` object.
- **`format`** - The format of the media object, eg. MP3, Ogg Vorbis (ogg), FLAC, or Ogg Opus (opus).
- **`url`**: The URL where the SongFile may be retrieved by a client.
- **`size`**: The size of the file in bytes
- **`sha256`**: The SHA256 digest of the file encoded in base64 without padding.
