from flask import Blueprint, jsonify, current_app, url_for
from .schemas import LicenseRepositorySchema, MusicRepositorySchema, \
     ArchiveSchema

api = Blueprint('api_v1', __name__,
                        template_folder='templates')

@api.route('licenses.json')
def license_repository():
    schema = LicenseRepositorySchema()
    return jsonify(schema.dump(current_app.db.license_repository))


@api.route('music.json')
def music_repository():
    schema = MusicRepositorySchema()
    return jsonify(schema.dump(current_app.db.music_repository))


@api.route('archive.json')
def archive_manifest():
    return jsonify({'licenses': url_for('api_v1.license_repository'),
                    'music': url_for('api_v1.music_repository')})

