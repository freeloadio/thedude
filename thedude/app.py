from pathlib import Path
from os import environ as env
from dotenv import load_dotenv
from flask import Flask, g, render_template, jsonify, abort, url_for, \
    send_file, Blueprint
from .models import Archive
from .api_v1 import api as api_v1

load_dotenv()
ARCHIVE_ROOT = Path(env.get('ARCHIVE_ROOT', 'archive')).resolve()

# Make a custom blueprint just for the static files
archive_bp = Blueprint('archive', __name__, static_folder=ARCHIVE_ROOT)

def make_db(archive_root: Path = Path(ARCHIVE_ROOT)):
    if 'db' in g:
        return g.db
    return Archive.from_path(archive_root)

def create_app():
    app = Flask(__name__,
                static_folder='assets')
    with app.app_context():
        app.db = make_db()
        app.register_blueprint(archive_bp, url_prefix='/media')
        app.register_blueprint(api_v1, url_prefix='/api/v1')
    return app

app = create_app()


@app.route("/")
def artist_list():
    artists = [app.db.music_repository.artists[artist]
                   for artist in \
               sorted(app.db.music_repository.artists.keys())]
    return render_template('index.html',
                           artists = artists)


@app.route('/artist/<artist_slug>/')
def artist_view(artist_slug: str):
    # Using the direct getter should be okay for a frozen site
    artist = app.db.music_repository.artists[artist_slug]
    return render_template('artist.html',
                           artist = artist)


@app.route('/album/<artist_slug>/<album_slug>/')
def album_view(artist_slug: str, album_slug: str):
    artist = app.db.music_repository.artists[artist_slug]
    album = artist.albums[album_slug]
    return render_template('album.html',
                           artist = artist,
                           album = album,
                           songs = album.songs.values())

