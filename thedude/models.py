from pathlib import Path
import json
import subprocess
from PIL import Image
from collections import defaultdict
from pydantic import BaseModel, HttpUrl
import hashlib
from base64 import b64encode

from typing import Optional, List, Dict
from markdown import markdown
import frontmatter
from slugify import slugify

def make_slug(s):
    return slugify(s,
                   max_length = 64,
                   stopwords = ('a', 'an', 'the'),
                   separator = '_')

def file_sha256(file_path: Path) -> str:
    BUF_SIZE = 65536 # 64k chunks
    sha256 = hashlib.sha256()
    with open(file_path, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha256.update(data)
    encoded_digest = b64encode(sha256.digest())
    return encoded_digest.decode('utf-8').rstrip("=")

# Define some "models"
# Define Pydantic models
class License(BaseModel):
    id: str
    name: str
    url: HttpUrl
    body: str
    
    def __str__(self):
        return self.id

    @classmethod
    def from_path(cls, license_path: Path):
        with open(license_path, 'r') as f:
            data = frontmatter.load(f)

        return cls(id=data['id'],
                   name=data['name'],
                   url=data['url'],
                   body=data.content)

class LicenseRepository(BaseModel):
    path: Path
    licenses: Dict[str, License] = {}
    songfiles: Dict[str, 'SongFile'] = {}
    
    @classmethod
    def from_path(cls, license_root: Path):
        licenses =  [License.from_path(license_path) \
                    for license_path in license_root.iterdir() \
                    if license_path.is_file() and license_path.suffix == '.mdx']
        return cls(path = license_root,
                   licenses = {license.id: license for license in licenses})

class SongFile(BaseModel):
    path: Path
    relative_path: Path
    format: str
    size: int
    sha256: str
    song: 'Song'

class Song(BaseModel):
    title: str
    tracknum: int = 0
    duration: float
    license_id: str = ''
    album: Optional['Album'] = None
    song_license: Optional[License] = None
    songfiles: Dict[str, SongFile] = {}
    
    @classmethod
    def from_path(cls, song_path: Path, album: Optional['Album'],
                  root_path: Path = Path('.')):
        size = song_path.stat().st_size
        sha256 = file_sha256(song_path)
        probes = {'.mp3': ffprobe_mp3,
                  '.ogg': ffprobe_ogg}
        try:
            fun = probes[song_path.suffix]
        except KeyError:
            raise ValueError("File type {song_path.suffix} probe not found/supported")
        sng = fun(song_path)
        # Lowercase the tags for easy access
        tags = {key.lower(): value for key,value in sng['tags'].items()}
        song = cls(title = tags['title'],
                   tracknum = int(tags['track']),
                   duration = sng['duration'],)
        songfile = SongFile(path = song_path,
                            relative_path = song_path.relative_to(root_path),
                            format = sng['format_name'],
                            song = song,
                            size = size,
                            sha256 = sha256
                            )
        song.songfiles[songfile.format] = songfile
        if album:
            song.album = album
        return song
    
    @property
    def slug(self):
        return make_slug(self.title)
    
    @property
    def license(self):
        if self.song_license:
            return self.license
        if self.album:
            return self.album.license

class Album(BaseModel):
    path: Path
    slug: str
    title: str
    description: str
    license_id: str
    license: Optional[License] = None
    artist: Optional['Artist'] = None
    songs: Dict[str, Song] = {}
    albumart: List['AlbumArt'] = []

    @classmethod
    def from_path(cls, album_path: Path, artist: Optional['Artist'] = None):
        albumfile = album_path.joinpath('album.mdx')
        with open(albumfile, 'r') as f:
            data = frontmatter.load(f)
        album = cls(path = album_path,
                      slug = album_path.stem,
                      title = data['title'],
                      license_id = data['license'],
                      description = markdown(data.content))
        if artist:
            album.artist = artist
        return album

    def collect_art(self, art_root: Optional[Path] = None,
                    relative_to: Optional[Path] = None):
        SUPPORTED_ART = ('cover.jpg', 'cover.jpeg', 'cover.png')
        if not art_root:
            art_root = self.path
        if not relative_to:
            relative_to = art_root.parents[0]
        # TODO: Support other types of art
        art = [ AlbumArt.from_path('cover', filename, root_path=relative_to) \
            for filename in art_root.iterdir() if filename.is_file() \
                and str(filename.name).lower() in SUPPORTED_ART]
        self.albumart = art

class Artist(BaseModel):
    slug: str
    name: str
    path: Path
    description: str
    albums: Dict[str, Album] = {}

    @classmethod
    def from_path(cls, artist_path: Path):
        artistfile = artist_path.joinpath('artist.mdx')
        with open(artistfile, 'r') as f:
            data = frontmatter.load(f)
        return cls(
            slug = artist_path.stem,
            path = artist_path,
            name = data['name'],
            description = markdown(data.content))

class MusicRepository(BaseModel):
    path: Path
    artists: Dict[str, Artist] = {}

    @classmethod
    def from_path(cls, music_root: Path):
        SUPPORTED_SUFFIX = ('.mp3', '.ogg', '.flac', '.vorbis', '.opus')
        parent = music_root.parents[0]
        # root / artist / album / song
        # Create the artists
        artists = [Artist.from_path(artist_path)
                   for artist_path in music_root.iterdir() \
                   if artist_path.is_dir()]
        # Create the albums
        for artist in artists:
            # Should we need to convert strings to paths?
            albums = {album_dir.stem: Album.from_path(album_dir, artist)
                      for album_dir in Path(artist.path).iterdir()
                      if album_dir.is_dir()}
            artist.albums = albums
            # Create the songs - this can be done breathe or depth, it doesn't matter
            for album in albums.values():
                # Should we need to convert strings to paths?
                songfiles = [parent.joinpath(sf) for sf in \
                             Path(album.path).iterdir()
                             if (sf.is_file() and sf.suffix in SUPPORTED_SUFFIX)]
                # Make the songfiles by filesystem order
                unsorted_songfiles = []
                for sf in songfiles:
                    song = Song.from_path(sf, album, parent)
                    unsorted_songfiles.append(song)
                # Sort songfiles by track number
                sorted_songfiles = sorted(unsorted_songfiles,
                                          key=lambda x: \
                                          getattr(x, 'tracknum') or 0)
                # Python dictionaries preserve insert order
                album.songs = {song.slug: song for song in sorted_songfiles}
                album.collect_art(relative_to = parent)
                    
        return cls(path = music_root,
                   artists = {artist.slug: artist for artist in artists})
    
    def reconcile_licenses(self, license_repository: LicenseRepository):
        licenses = license_repository.licenses
        for artist in self.artists.values():
            for album in artist.albums.values():
                try:
                    album.license = licenses[album.license_id]
                except KeyError:
                    raise KeyError(f"Album {album.slug} references license {album.license_id} which is not in license database")
                # Now check for varied licenses
                if album.license == 'VARIED':
                    if not all(bool(song.license_id) in album.songs):
                        raise ValueError(
                            f"Not all songs in {album.title} have a license")
                for song in album.songs.values():
                    if song.license_id:
                        song.song_license = licenses[song.license_id]

class AlbumArt(BaseModel):
    path: Path
    relative_path: Path
    role: str
    format: str
    height: int
    width: int
    size: int
    sha256: str

    @classmethod
    def from_path(cls, role: str,
                  art_path: Path,
                  root_path: Path = Path('.')):
        size = art_path.stat().st_size
        sha256 = file_sha256(art_path)
        im = Image.open(art_path)
        width, height = im.size
        fmt = im.format.lower()
        return cls(path = art_path,
                   relative_path = art_path.relative_to(root_path),
                   role = role,
                   format = fmt,
                   height = height,
                   width = width,
                   size = size,
                   sha256 = sha256)


class Archive(BaseModel):
    archive_root: Path
    name: str
    description: str
    url: HttpUrl
    music_repository: MusicRepository
    license_repository: LicenseRepository
    
    @classmethod
    def from_path(cls, archive_root: Path):
        repofile = archive_root.joinpath('archive.mdx')
        with open(repofile, 'r') as f:
            repo_data = frontmatter.load(f)
        licenses = LicenseRepository.from_path(archive_root.joinpath('licenses'))
        music = MusicRepository.from_path(archive_root.joinpath('music'))
        music.reconcile_licenses(licenses)
        return cls(archive_root = archive_root,
                   name = repo_data['name'],
                   url = repo_data['url'],
                   description = repo_data.content,
                   license_repository = licenses,
                   music_repository = music)
    
# To resolve forward references
SongFile.update_forward_refs()
Song.update_forward_refs()
Album.update_forward_refs()
AlbumArt.update_forward_refs()
    
def ffprobe_mp3(song_path: Path | str):
    # ffprobe -v quiet -print_format json -show_format -show_streams somefile.asf
    output =  subprocess.check_output(
        ['ffprobe', '-v', 'quiet', '-print_format', 'json', '-show_format',
         '-show_streams', str(song_path)], text=True)
    song_data = json.loads(output)
    return song_data['format']

def ffprobe_ogg(song_path: Path | str):
    # ffprobe -v quiet -print_format json -show_format -show_streams somefile.asf
    output =  subprocess.check_output(
        ['ffprobe', '-v', 'quiet', '-print_format', 'json', '-show_format',
         '-show_streams', str(song_path)], text=True)
    song_data = json.loads(output)
    return {'format_name': song_data['format']['format_name'],
            'duration': song_data['format']['duration'],
            **song_data['streams'][0]}
