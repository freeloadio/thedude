from flask_frozen import Freezer
from .app import app

freezer = Freezer(app)
app.config['FREEZER_DESTINATION'] = '../public'

def freeze():
    freezer.freeze()

if __name__ == '__main__':
    freezer.freeze()
