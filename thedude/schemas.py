from marshmallow import Schema, fields, post_load, pre_dump, pre_load, post_dump, EXCLUDE
from flask import url_for, current_app
from .models import Song, Artist, Album, MusicRepository, License, LicenseRepository, Archive

class Typeable:
    # Add the static "type" field during serialization
    @post_dump
    def add_type(self, data, many, **kwargs):
        data['type'] = self.type
        return data

class TypeableSchema(Schema, Typeable):
    pass

class AlbumArtSchema(TypeableSchema):
    type = "AlbumArt"
    url = fields.Method("get_url")
    path = fields.Str()
    relative_path = fields.Str()
    role = fields.Str()
    format = fields.Str()
    height = fields.Int()
    width = fields.Int()
    size = fields.Int()
    sha256 = fields.Str()
    
    class Meta:
        exclude = ('path', 'relative_path')

    def get_url(self, obj):
        with current_app.app_context():
            return url_for('archive.static', 
                           filename=obj.relative_path)
        
class SongFileSchema(TypeableSchema):
    type = "SongFile"
    url = fields.Method("get_url")
    path = fields.Str()
    relative_path = fields.Str()
    format = fields.Str()
    size = fields.Int()
    sha256 = fields.Str()

    class Meta:
        exclude = ('path', 'relative_path')

    def get_url(self, obj):
        with current_app.app_context():
            return url_for('archive.static', 
                           filename=obj.relative_path)


class SongSchema(TypeableSchema):
    type = "Song"
    title = fields.Str(required=True)
    tracknum = fields.Int(required=False)
    duration = fields.Float()
    album_slug = fields.Str(required=False, allow_none=True)
    song_license = fields.Str(required=False)
    slug = fields.Str(required=True)
    album_slug = fields.Method("get_album_slug")
    artist_slug = fields.Method("get_artist_slug")
    songfiles = fields.Dict(keys=fields.Str(),
                            values=fields.Nested(SongFileSchema))
    class Meta:
        exclude = ['song_license']

    def get_slug(self, obj):
        return obj.slug

    def get_album_slug(self, obj):
        return obj.album.slug

    def get_artist_slug(self, obj):
        return obj.album.artist.slug
    
    def get_license(self, obj):
        return obj.license.id

    @post_load
    def make_song(self, data, **kwargs):
        return Song(**data)

    @post_dump
    def serialize_songfiles_as_list(self, data, **kwargs):
        if isinstance(data['songfiles'], dict):
            data['songfiles'] = list(data['songfiles'].values())
        return data

class AlbumSchema(TypeableSchema):
    type = "Album"
    path = fields.Str(required=True)
    slug = fields.Str(required=True)
    title = fields.Str(required=True)
    description = fields.Str(required=True)
    license = fields.Str(required=True)
    single = fields.Bool(required=False)
    songs = fields.Dict(keys=fields.Str(),
                        values=fields.Nested(SongSchema))
    cover_url = fields.Str(required=False)
    artist_slug = fields.Method("get_artist_slug")
    albumart = fields.List(fields.Nested(AlbumArtSchema, allow_none=False))
    
    class Meta:
        exclude = ['path']

    def get_artist_slug(self, obj):
        return obj.artist.slug

    @post_load
    def make_album(self):
        return Album(**data)

    @post_dump
    def serialize_songs_as_list(self, data, **kwargs):
        if isinstance(data['songs'], dict):
            data['songs'] = list(data['songs'].values())
        return data

class ArtistSchema(TypeableSchema):
    type = "Artist"
    slug = fields.Str(required=True)
    name = fields.Str(required=True)
    path = fields.Str(required=True)
    description = fields.Str(required=True)
    albums = fields.Dict(keys=fields.Str(),
                         values = fields.Nested(AlbumSchema),
                         required=True)

    class Meta:
        exclude = ['path']

    @post_load
    def make_artist(self, data, **kwargs):
        return Artist(**data)

    @post_dump
    def serialize_albums_as_list(self, data, **kwargs):
        if isinstance(data['albums'], dict):
            data['albums'] = list(data['albums'].values())
        return data

class LicenseSchema(TypeableSchema):
    type = "License"
    id = fields.Str(required=True)
    name = fields.Str(required=True)
    url = fields.Url(required=True)
    body = fields.Str(required=True)

    @post_load
    def make_license(self, data, **kwargs):
        return License(**data)

class LicenseRepositorySchema(Schema):
    path = fields.Str(required=True)
    licenses = fields.Dict(keys=fields.Str(),
                           values=fields.Nested(LicenseSchema),
                           required=True)

    class Meta:
        exclude = ['path']

    @post_dump
    def serialize_licenses_as_list(self, data, **kwargs):
        if isinstance(data['licenses'], dict):
            data['licenses'] = list(data['licenses'].values())
        return data

    @post_load
    def make_license_repository(self, data, **kwargs):
        return LicenseRepository(**data)

class MusicRepositorySchema(Schema):
    path = fields.Str(required=True)
    artists = fields.Dict(keys=fields.Str(),
                          values = fields.Nested(ArtistSchema))

    class Meta:
        exclude = ['path']

    @post_load
    def make_music_repository(self, data, **kwargs):
        artists_list = data.get('artists', [])
        data['artists'] = {artist['slug']: artist for artist in artists_list}
        return MusicRepository(**data)

    @post_dump
    def serialize_artists_as_list(self, data, **kwargs):
        if isinstance(data['artists'], dict):
            data['artists'] = list(data['artists'].values())
        return data

class ArchiveSchema(TypeableSchema):
    type = "Archive"
    archive_root = fields.Str(required=True)
    name = fields.Str(required=True)
    description = fields.Str(required=True)
    url = fields.Url(required=True)
    music_repository = fields.Nested(MusicRepositorySchema, required=True)
    license_repository = fields.Nested(LicenseRepositorySchema, required=True)

    @post_load
    def make_repository(self, data, **kwargs):
        return Archive(**data)
