The Dude: A Prototype Freeloadio Server
========================================

The Dude is a Freeloadio Music Archive Server

Installation
--------------

Installing thedude is as easy as cloning the repo and running `poetry install`

Media Archive
---------------

This software assumes you have another web server serving the media archive. Currently the easiest way to do that is to have an `archive` directory under the project directory (`thedude`) and then run:

`python -m http.server -d archive`

Building and Running
---------------------

This software can be either run as is, or build as a static site. To run it, from the project directory run:

`poetry run flask --app=thedude run`

To build it, run:

`poetry run build`

and then serve the `public/` directory.

License
--------

The Dude Copyright Three Tablets LLC, available under the Apache 2.0 International License

Simple.css Copyright Kevin Quirk, available under the MIT License.
